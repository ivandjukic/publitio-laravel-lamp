<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Documentation</title>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div>
            <article>
                <h1>Publio LAMP documentation</h1>
                <h3>1. Create ssh key on local pc</h3>
                <pre>
                    <code>ssh-keygend</code>
                </pre>
                <h3>2. Add ip address to /etc/hosts for easier access</h3>
                <pre>
                    <code>159.65.201.169  publitio-test-server</code>
                </pre>
                <h3>3. Copy ssh key to server</h3>
                <pre>
                    <code>ssh-copy-id -i publitio.pub root@publitio-test-server</code>
                </pre>
                <h3>4. Get ubuntu version</h3>
                <pre>
                    <code>lsb_release -a</code>
                </pre>
                <h3>5. Update existing sofware and fetch new version of packages</h3>
                <pre>
                    <code>apt-get update && apt-get upgrade -y; </code>
                </pre>
                <h3>6. Install apache2</h3>
                <pre>
                    <code>apt install apache2 -ya</code>
                </pre>
                <h3>7. Edit firewall settings, allow apache</h3>
                <pre>
                    <code>ufw allow 'Apache'</code>
                </pre>
                <h3>8. Test apache</h3>
                <pre>
                    <code>curl localhost:80</code>
                </pre>
                <h3>9. Install mysql server</h3>
                <pre>
                    <code>apt install mysql-server -y</code>
                </pre>
                <h3>10. Secure Mysql installation</h3>
                <pre>
                    <code>mysql_secure_installation (select N(no) for pass validation plugin and Y(yes) for all other options)</code>
                </pre>
                <h3>11. Access mysql, set root password, create new user for localhost and remote access, set password
                    for
                    new user and flush privileges</h3>
                <pre>
                    <code>
                        mysql;
                        ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root12345';
                        CREATE USER 'publitio'@'localhost' IDENTIFIED BY 'publitio12345';
                        CREATE USER 'publitio'@'%' IDENTIFIED BY 'publitio12345';
                        FLUSH PRIVILEGES;
                    </code>
                </pre>
                <h3>12. Allow remote access to mysql</h3>
                <pre>
                    <code>
                        at /etc/mysql/mysql.conf.d/mysqld.cnf replace
                            bind-address           = 127.0.0.1
                        with
                            #bind-address           = 127.0.0.1</code>
                </pre>
                <h3>13. Restart mysql server to apply changes</h3>
                <pre>
                    <code>systemctl restart mysql</code>
                </pre>
                <h3>14. Test login to mysql from local for both users</h3>
                <pre>
                    <code>
                        mysql -u root -proot12345
	                    mysql -u publitio -ppublitio12345
                    </code>
                </pre>
                <h3>15. Test login to mysql from remote computer(Only allowed for publitio)</h3>
                <pre>
                    <code>mysql -u publitio -ppublitio12345 -h159.65.201.169 -P 3306</code>
                </pre>
                <h3>16. Install php</h3>
                <pre>
                    <code>apt install php libapache2-mod-php -y</code>
                </pre>
                <h3>17. Install some required php dependencies</h3>
                <pre>
                    <code>apt-get install php-pear php7.2-curl php7.2-dev php7.2-gd php7.2-mbstring php7.2-zip php7.2-mysql php7.2-xml php-token-stream php7.2-json -y</code>
                </pre>
                <h3>18. Install Git</h3>
                <pre>
                    <code>apt-get install git -y</code>
                </pre>
                <h3>19. Install composer</h3>
                <pre>
                    <code>apt-get install composer -y</code>
                </pre>
                <h3>20. Clone project from gitlab into /var/www</h3>
                <pre>
                    <code>
                        cd /var/www
 	                    git clone https://gitlab.com/ivandjukic/publitio-laravel-lamp.git (I did't use ssh because the project is public)
                    </code>
                </pre>
                <h3>21. Install project dependencies</h3>
                <pre>
                    <code>
                        cd publitio-laravel-lamp (/var/www/publitio-laravel-lamp)
	                    composer install
                    </code>
                </pre>
                <h3>22. Copy config example file to .env</h3>
                <pre>
                    <code>cp .env.example .env</code>
                </pre>
                <h3>23. Set right ownership and permissions</h3>
                <pre><code>
                        chown -R www-data: storage
                        sudo chmod -R 755 storage
                        sudo find . -type f -exec chmod 644 {} \;
                        find . -type d -exec chmod 755 {} \;
                        chgrp -R www-data storage
                        chmod -R ug+rwx storage
                 </code></pre>
                <h3>24. Create new apache2 config file</h3>
                <pre>
                    <code>cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/foo.conf</code>
                </pre>
                <h3>25. Edit foo.conf</h3>
                <pre>
                    <code>
                        &lt;VirtualHost *:80&gt;

                            ServerAdmin ivandjukicivan@hotmail.com
                            ServerName 3.8.132.139
                            ServerAlias 3.8.132.139
                            DocumentRoot /var/www/publitio-laravel-lamp/public
                            ErrorLog /error.log
                            CustomLog /access.log combined

                            &lt;Directory /var/www/publitio-laravel-lamp/public&gt;
                                Options Indexes FollowSymLinks
                                AllowOverride All
                                Require all granted
                            Order allow,deny
                            Allow from all
                            &lt;/Directory&gt;

                            RewriteEngine on

                        &lt;/VirtualHost&gt;
                    </code>
                </pre>
                <h3>26. Enalble rewrite mode for apache2</h3>
                <pre>
                    <code>a2enmod rewrite</code>
                </pre>
                <h3>27. Disable default vhost config</h3>
                <pre>
                    <code>a2dissite 000-default.conf </code>
                </pre>
                <h3>28. Enable new foo.conf</h3>
                <pre>
                    <code>a2ensite foo.conf</code>
                </pre>
                <h3>29. Test apache configuration</h3>
                <pre>
                    <code>apache2ctl configtest</code>
                </pre>
                <h3>30. Restart appache to apply changes</h3>
                <pre>
                    <code>service apache2 reload</code>
                </pre>


            </article>
        </div>
    </div>
</div>
</body>
</html>
