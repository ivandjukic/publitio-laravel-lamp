<?php
header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

Route::middleware('cors')->group(function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@authenticate');


    Route::group(['middleware' => ['jwt.verify']], function () {
        Route::get('user', 'UserController@getAuthenticatedUser');
        Route::get('people', 'PeopleController@get');
        Route::post('people', 'PeopleController@create');
        Route::delete('people/{id}', 'PeopleController@delete');
        Route::get('profile/{id}', 'ProfileController@getProfileById');
        Route::get('isEmailTaken/{email}', 'PeopleController@isEmailTaken');
        Route::post('email', 'EmailController@send');
    });

});