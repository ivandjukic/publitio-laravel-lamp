<?php

namespace App\Providers;

use App\Photo;
use Intervention\Image\ImageManagerStatic as Image;

/**
 * Class PhotoService
 * @package App\Providers
 */
class PhotoService
{
    const PROFILE_PATH = "profile-photos";
    const QUALITY = "100";

    /**
     * @param $file
     * @return Photo
     */
    public static function uploadPhoto($file): Photo
    {
        $photo = new Photo();
        $photo->name = self::UploadFile($file);
        $photo->type = image_type_to_extension(exif_imagetype($file));
        $photo->save();
        return $photo;
    }

    /**
     * @param $file
     * @return string
     */
    public static function UploadFile($file)
    {
        $image_name = sha1($file->getClientOriginalName() . microtime(true));
        $image_object = Image::make($file);
        $image_object->save(
            $_SERVER['DOCUMENT_ROOT'] . '/' . self::PROFILE_PATH . '/' . $image_name . '.' . basename($image_object->mime()),
            self::QUALITY
        );
        return $image_name;
    }
}
