<?php

namespace App\Providers;

use Illuminate\Support\Facades\Mail;

/**
 * Class EmailService
 * @package App\Providers
 */
class EmailService
{
    /**
     * @param array $data
     */
    public static function sendEmail(array $data)
    {
        $to_name = $data['receiver_name'];
        $to_email = $data['receiver_email'];
        $subject = $data['subject'];
        $data = array('content' => $data['content']);
        $email_from_name = 'Ivan DJukic';
        $email_from_email = 'test1ivantest@gmail.com';
        Mail::send('emails.mail', $data, function ($message) use ($to_name, $to_email, $subject, $email_from_email, $email_from_name) {
            $message->to($to_email, $to_name)
                ->subject($subject);
            $message->from($email_from_email, $email_from_name);
        });
    }
}