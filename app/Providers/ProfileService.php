<?php

namespace App\Providers;

use App\Profile;

/**
 * Class ProfileService
 * @package App\Providers
 */
class ProfileService
{
    /**
     * @param array $data
     * @return Profile
     * @throws \Throwable
     */
    public static function create(array $data): Profile
    {
        $photo = PhotoService::uploadPhoto($data['photo']);
        $profile = new Profile();
        $profile->name = $data['name'];
        $profile->email = $data['email'];
        $profile->description = isset($data['description']) ? $data['description'] : null;
        $profile->photo_id = $photo->id;
        $profile->saveOrFail();
        $profile->photo = $photo;
        return $profile;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public static function get(array $data): array
    {
        $profiles = Profile::orderBy('id', 'DESC');
        if (isset($data['limit'])) {
            $profiles->limit($data['limit']);
        }
        if (isset($data['offset'])) {
            $profiles->offset($data['offset']);
        }
        return [
            'profiles' => $profiles->get(),
            'total' => Profile::count()
        ];
    }

    /**
     * @param int $id
     * @return Profile
     */
    public static function delete(int $id): Profile
    {
        $profile = Profile::findOrFail($id);
        $profile->delete();
        return $profile;
    }

    /**
     * @param string $email
     * @return bool
     */
    public static function isEmailTaken(string $email): bool
    {
        return Profile::where('email', $email)->exists();
    }

    /**
     * @param int $id
     * @return Profile
     */
    public static function getProfile(int $id): Profile
    {
        return Profile::findOrFail($id);
    }
}
