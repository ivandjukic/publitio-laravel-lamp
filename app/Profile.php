<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'name', 'email', 'description'
    ];


    public function photo()
    {
        return $this->hasOne('App\Photo', 'id', 'photo_id');
    }

    protected $with = [
        'photo'
    ];


}
