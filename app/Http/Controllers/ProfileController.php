<?php

namespace App\Http\Controllers;

use App\Http\Requests\TestRequest;
use App\Providers\ProfileService;

/**
 * Class ProfileController
 * @package App\Http\Controllers
 */
class ProfileController extends Controller
{
    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProfileById(int $id)
    {
        try {
            return response()->json(ProfileService::getProfile($id, $id));
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'invalid profile id'
            ], 400);
        }
    }

}
