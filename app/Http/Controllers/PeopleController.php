<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProfileRequest;
use App\Http\Requests\TestRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Profile;
use App\Providers\ProfileService;
use Illuminate\Http\Request;

/**
 * Class PeopleController
 * @package App\Http\Controllers
 */
class PeopleController extends Controller
{

    /**
     * @param CreateProfileRequest $request
     * @return Profile
     */
    public function create(CreateProfileRequest $request)
    {
        $request->validated();
        return response()->json(ProfileService::create($request->only('name', 'email', 'description', 'photo')));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(Request $request)
    {
        return response()->json(ProfileService::get($request->only('limit', 'offset')));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            return response()->json(
                ProfileService::delete($id)
            );
        } catch (\Exception $e) {
            return response()->json([
                'error' => 'invalid profile id'
            ], 400);
        }

    }

    /**
     * @param string $email
     * @return \Illuminate\Http\JsonResponse
     */
    public function isEmailTaken(string $email)
    {
        return response()->json([
            'taken' => ProfileService::isEmailTaken($email)
        ], 200);
    }


}
