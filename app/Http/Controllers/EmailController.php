<?php

namespace App\Http\Controllers;

use App\Http\Requests\SendEmailRequest;
use App\Providers\EmailService;

/**
 * Class EmailController
 * @package App\Http\Controllers
 */
class EmailController extends Controller
{
    /**
     * @param SendEmailRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function send(SendEmailRequest $request)
    {
        $request->validated();
        try {
            EmailService::sendEmail($request->only('subject', 'content', 'receiver_email', 'receiver_name'));
        } catch (\Exception $e) {
            return response()->json('error', 500);
        }
        $request->validated();
    }
}